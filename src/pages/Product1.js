import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
// import AdminDash from "../pages/AdminDash";
import { useContext, useEffect, useState } from 'react';
import UserContext from '../UserContext';

export default function ProductPage() {

	
	const [ allProducts, setAllProducts ] = useState([])

	const fetchData = () => {
		fetch('https://pop-shopp-api.onrender.com/products/allProducts')
		.then(res => res.json())
		.then(data => {
			setAllProducts(data)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])

	
	const { user } = useContext(UserContext);

	return(
		<>
			


			{(user.isAdmin === true && user.id !== null) ?

				<AdminView productsData={allProducts} fetchData={fetchData}/>
				:
				<>
				<UserView productsData={allProducts} />
				</>
			}
			

		</>


		)
}

//ok