import {useState, useEffect} from 'react';
import OrderCard from '../components/OrderCard';
// import UserContext from '../UserContext';


export default function UserOrders () {

	// const { user } = useContext(UserContext);

	const [orders,setOrders] = useState([])
	const token = localStorage.getItem("token")

	useEffect(()=>{
		fetch("https://pop-shopp-api.onrender.com/orders/getUserOrders",{
			method: "GET",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data);

			setOrders(data.map(order=>{
				return(
					<OrderCard key={order._id} orderProp = {order}/>
				)
			}))


		})

	})
	
	return(
		<>
			<h1 style={{textAlign: "center"}}>My Orders:</h1>
			{orders}
		</>
		)
}

//ok