// import {useEffect,useState,useContext} from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import UserContext from '../UserContext';


export default function Home() {
  // const {user} = useContext(UserContext);

    // const [user, setUser] = useState({
    //     firstName: null,
    //     lastName: null,
    //     email: null,
    //     mobileNo: null,
    //     isAdmin: null,
    //     _id: null

    // })

    // useEffect(()=>{
    // fetch('http://localhost:4000/users/getUserDetails',{
    //   headers: {
    //     Authorization: `Bearer ${localStorage.getItem('token')}`
    //   }
    // })
    // .then(res => res.json())
    // .then(data=> {
    //   console.log(data);
    //   setUser({
    //     _id: data._id,
    //     firstName: data.firstName,
    //     lastName: data.lastName,
    //     email: data.email,
    //     mobileNo: data.mobileNo,
    //     isAdmin: data.isAdmin
    //   })
    // })
    // },[])





	const data = {
        // title: `Hello! ${user.firstName}!`,
        title:`Hello, Poppshopper!`,
        content: `Shop now, shop more!`,
        destination1: "/products",
        label1: "shopping_bag",
        l1: "Products",
        l2: "My Account",
        l3: "New Products!",
        l4: "Exclusive Products",
        destination2: "/profile",
        label2:"Account_Circle",
        destination3: "/products",
        label3: "storefront",
        destination4: "/exclusives",
        label4:"star",
        image: "./img/5.png"
    }

    return (
        <>
	        <Banner data={data}/>
	        <Highlights />
		</>

    )
}

//ok