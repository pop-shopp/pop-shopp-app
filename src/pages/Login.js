import {useState, useEffect, useContext} from 'react';
import {Form,Button,Card} from 'react-bootstrap';
import {Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login (){
	const {user, setUser} = useContext(UserContext);
	console.log(user);
	const [email, setEmail] = useState ('');
	const [password, setPassword] = useState ('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);

	function loginUser(e){
		e.preventDefault();
		fetch('https://pop-shopp-api.onrender.com/users/login',{
			method: 'POST',
			headers:{
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})		
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			if(typeof data.accessToken !== "undefined"){
				localStorage.setItem('token', data.accessToken)
				retrieveUserDetails(data.accessToken);
				
				Swal.fire({
					title:"Login Sucessful!",
					icon:"success",
					text:"Welcome to PoppShopp!"
				})

			}else{
				Swal.fire({
					title:"Authentication Failed",
					icon: "error",
					text: "Check your credentails!"
				});
			};

		});

		setEmail('');
		setPassword('');
	};

	const retrieveUserDetails = (token) => {
		fetch('https://pop-shopp-api.onrender.com/users/getUserDetails', {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			setUser({
				id:data._id,
				isAdmin:data.isAdmin
			});
		})
	}

	useEffect (() => {
		
		if(email !== '' && password !==''){
			setIsActive(true);
		}else{
			setIsActive(false);
		};

	}, [email, password]);

	return(
		(user.id !== null)?
			<Navigate to="/products"/>
		:
		<>
		<h1 id="font-link" style={{textAlign: "center"}}>PoppShopp!</h1>
		<h5 style={{textAlign: "center"}}>Login</h5>
		<Form onSubmit={e=>loginUser(e)} >
			<Form.Group controlId="userloginEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					className="colorForm"
					type="email"
					placeholder="Enter your email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
					/>
					<Form.Text className="text-muted">
						<i>Enter your registered email.</i>
					</Form.Text>
			</Form.Group>

			<Form.Group controlId="userPassword">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here!"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
					/>
					<Form.Text className="text-muted">
						<i>Forgot Password?</i>
					</Form.Text>
			</Form.Group>
			<p>Not yet registered? <Link to="/register">Register Here</Link></p>

		{ isActive ?
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submit_Btnlogin">
				Login
			</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submit_Btnlogin" disabled>
				Login
			</Button>
		}

		<Card className = "cardHighlight">
			<Card.Body className= "colorNavbar">
				<Card.Title>
					<p style={{textAlign: "center"}}>Not yet registered?</p>
				</Card.Title>
				<Card.Text>
				<p style={{textAlign: "center"}}>Don't have an account? <Link to="/register">Register Here</Link></p>
				</Card.Text>
			</Card.Body>
		</Card>
		</Form>		
		</>
		)
}

//ok