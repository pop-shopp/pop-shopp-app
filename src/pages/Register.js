import {useState, useContext} from 'react';
import {useEffect} from 'react';
import {Form, Row,Col,Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register (){
	
	/*const [emailcount, emailsetCount] = useState('');
	const [pwcount1, setpwCount1] = useState('');
	const [pwcount2, setpwCount2] = useState('');
	const [isActive, setIsActive] = useState(false);
	*/
	const {user} = useContext(UserContext);
	// const {user,setUser} = useContext(UserContext);
	console.log(user);
	const history = useNavigate();

	//state hooks to store the values of the input fieleds
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState ('');
	const [password, setPassword] = useState ('');
	// const [password2, setPassword2] = useState ('');
	//state to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	//check if all values from are successfully binded
	// console.log(email);
	// console.log(password);
	// console.log(password2);


	function registerUser(e){
		//prevents page redirection via form submission
		e.preventDefault();

		fetch('https://pop-shopp-api.onrender.com/users/checkEmailExists',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
			.then(res=> res.json())
			.then(data =>{
				console.log(data)
				//result is boolean

				if(data){
					Swal.fire({
						title: "Duplicate email found!",
						icon: "info",
						text: "The email that you are trying to register already exists!"
					})
				}else{
					fetch('https://pop-shopp-api.onrender.com/users',{
						method:'POST',
						headers:{
							'Content-Type':'application/json'
						},
						body:JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							mobileNo: mobileNo,
							email:email,
							password:password
						})
					})
				.then(res=> res.json())
				.then(data=>{
					console.log(data)

					if(data.email){
						Swal.fire({
							title: 'PoppShopp Registration Successful!',
							icon: 'success',
							text: 'Thank you for registering!'
						});
						history("/login")
					}else{
						Swal.fire({
							title: 'Registration failed!',
							icon: 'error',
							text: 'Something went wrrong, try again!'
						})
					}
				})
			};
		});

		// localStorage.setItem("email",email);
		//clear input fields
		setFirstName('');
		setLastName('');
		setMobileNo('');
		setEmail('');
		setPassword('');
		// setUser({
		// 	email: localStorage.getItem('email')
		// });
		// alert('Thank you for registering!')
	}

	//hook
	//Syntax

		//useEffect(() => {},[])
		//validation to enable the submit button when all fields are populated and both passwords match
	useEffect (() => {
		
		// if((email !== '' && password1 !=='' && password2 !=='') && (password1===password2)){
		// 	setIsActive(true);
		// }
		if(email !== '' && firstName !=='' && lastName !== '' && password !== '' && mobileNo.length===11){
			setIsActive(true);
		}else{
			setIsActive(false);
		};

	}, [email, firstName, lastName, mobileNo, password]);
 
	//sa [] ang changes, they are called dependencies
	//pag wala si dependencies di gagana?

	return(
		(user.id !== null)?
			<Navigate to="/products"/>
		:
		<>
		<h1 id="font-link" style={{textAlign: "center"}}>PoppShopp!</h1>
		<h5 style={{textAlign: "center"}}>Register</h5>
		<Form onSubmit={e=>registerUser(e)}>
			<Row>
			<Col>
			<Form.Group controlId="userFirstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your full name here"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					/>
			</Form.Group>
			</Col>
			<Col>
			<Form.Group controlId="userLastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your last name here"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					/>
			</Form.Group>
			</Col>
			</Row>
			<Row>
			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your 11-digit mobile number here"
					required
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					/>
					<Form.Text className="text-muted">
						We'll never share your mobile number with anyone else.
					</Form.Text>
			</Form.Group>
			</Row>
			<Row>
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter your email here"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
					/>
					<Form.Text className="text-muted">
						We'll never share your email with anyone else.
					</Form.Text>
			</Form.Group>
			</Row>
			<Row>
			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
					/>
			</Form.Group>
			</Row>
			<Row>
		{ isActive ?
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submit_Btn">
				Register
			</Button>
			:
			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submit_Btn" disabled>
				Register
			</Button>
		}
			</Row>
		</Form>		
		</>
		)
}

/*controlID for input field siya ang nagpoprroduce*/

//ok