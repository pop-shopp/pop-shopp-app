import {useState, useEffect} from 'react';
import OrderCard1 from '../components/OrderCard1';

export default function Orders () {

	const [orders,setOrders] = useState([])
	const token = localStorage.getItem("token")

	useEffect(()=>{
		fetch("https://pop-shopp-api.onrender.com/orders",{
			method: "GET",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			// console.log(data);

			setOrders(data.map(order=>{
				return(
					<OrderCard1 key={order._id} orderProp = {order}/>
				)
			}))


		})

	},[token])
	
	return(
		<>
			<h1 style={{textAlign: "center"}}>All Orders:</h1>
			{orders}
		</>
		)
}
//ok