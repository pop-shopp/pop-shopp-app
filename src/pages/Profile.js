import {useState, useEffect} from 'react'
import {Button, Container, Card} from 'react-bootstrap'
import {Link} from 'react-router-dom';
// import Order1 from '../pages/Order';


export default function Profile() {

    const [user1Details, setUser1Details] = useState({
        firstName: null,
        lastName: null,
        email: null,
        mobileNo: null,
        isAdmin: null,
        _id: null

    })

    useEffect(()=>{
    fetch('https://pop-shopp-api.onrender.com/users/getUserDetails',{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data=> {
      console.log(data);
      setUser1Details({
        _id: data._id,
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        mobileNo: data.mobileNo,
        isAdmin: data.isAdmin
      })
    })
    },[])

    return(
        <div
            className="profileBg"
            style={{height: 500}}
        >
        <Container className="pt-5">
        <Card >
              <Card.Header as="h5">Profile</Card.Header>
              <Card.Body className="colorPurple">
                <Card.Title className="mb-3">User Details</Card.Title>
                <Card.Text>
                  First Name: {user1Details.firstName} 
                </Card.Text>
                <Card.Text>
                  Last Name: {user1Details.lastName} 
                </Card.Text>
                <Card.Text>
                  Email: {user1Details.email}  
                </Card.Text>
                 <Card.Text>
                  MobileNo: {user1Details.mobileNo}  
                </Card.Text>
               {/* <Card.Text>
                Admin Tools Available: {userDetails.isAdmin}
                </Card.Text>*/}
              </Card.Body>
                    {
                   (user1Details.isAdmin === false) ?
                   <>
                    <div className="colorNavbar text-center">
                    <Button  className="m-5" size="sm" style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }} variant="outline-dark" as= {Link} to={`/myorders`}><span class=" px-5 material-symbols-rounded">history</span><p>Order History</p></Button>

                   {/* <Button  disabled="true"className="m-5" size="sm" style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }} variant="outline-dark" as= {Link} to={`/mycart`}><span class=" px-5 material-symbols-rounded">shopping_cart</span><p>My Cart, Coming Soon</p></Button>*/}
                    </div> 
                   </>
                   :
                   <>
                    <div className="text-center">
                  {/*  <Link className="btn-outline-dark" to="orders/orders">All Orders</Link>*/}
                    <Button  className="me-5" size="sm" style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }} variant="outline-dark" as= {Link} to={`/orders`}><span class="material-symbols-rounded">search</span>View All Orders</Button>
  {/*                  <Link className="btn btn-danger" to="/login">Log In</Link>}*/}
                    </div>
                   </>}
            </Card>
         </Container>
        
        </div>
    )
}

//ok