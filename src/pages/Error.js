import Banner from '../components/Banner';

export default function Error() {

    const data = {
        title: "Error! Under Maintenance!",
        content: "The page you are looking for cannot be found",
        destination1: "/",
        label1: "home",
        destination2: "/products",
        label2:"shopping_bag",
        destination3: "/register",
        label3: "app_registration",
        destination4: "/login",
        label4:"login",
        image: "./img/3.png",
        l1: "Back to Home",
        l2: "",
        l3: "",
        l4: ""
    }
    
    return (
        <Banner data={data}/>
    )
}

//ok