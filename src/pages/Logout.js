import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){
	
	const {unsetUser, setUser} = useContext(UserContext);
	
	//clear the localStorage of the user's info
	unsetUser();

	// localStorage.clear();

	//hindi nagrerender si app navbar kay gamitin natin si useEffect
	//placing the setuser function inside of the useEffect is necessary beacuse of updates within reactJS that a state of another component cannot be updated while trying to render a diff component

	//by adding useEffect, this will allow the logout page to render first before triggering the useEffect which changes the state of the user

	useEffect(() =>{
		//set the user state back to its original state
		//clear out muna tapos set the user back to original state
		setUser({id:null})
	})

	return(
		<Navigate to="/login"/>

		)
};

//ok