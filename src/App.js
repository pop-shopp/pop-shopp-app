import {useState,useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router,Route,Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';
// import AdminViewp from './components/AdminViewp'
// import AddProduct from './components/AddProduct';
// import CartView from './components/CartView';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Exclusives from './components/Exclusives';
import ViewUsers from './components/ViewUsers';
import UserOrder1 from './pages/UserOrder1';
import Order1 from './pages/Order1';
import Product1 from './pages/Product1';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import Profile from './pages/Profile'
// import Order from './pages/Order'
import './App.css';
import {UserProvider} from './UserContext';
//kailangan natin iimport para maconsume yung data
function App() {

	//stake hook for the user state that defoned here for a global scope
	//pwede natin ilgay kaahit saan
	//global scope pwede maccess kahit ssan
	//initialized as an object with properties from the localStorage
	//this will be used to store the suer info and will be used for validating if a user is logged in on the app or not
	//login (kung admin or regular user)
		//localStorage store nya token ni user
	const [user, setUser] = useState ({
		// email: localStorage.getItem('email')
		id: null,
		isAdmin: null
	});
	//logout
	const unsetUser = () => {
		localStorage.clear();
	};

	//if properly stored ang ating info

	useEffect(()=>{
		fetch('https://pop-shopp-api.onrender.com/users/getUserDetails',{
			headers:{
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			//captured the data of whoever is logged in
			console.log(data);

			if(typeof data._id !=="undefined"){
				setUser({
					id: data._id,
					isAdmin: data.isAdmin,
					firstName:data.firstName
				})
			}else {
				//set back the initial state of user
				setUser({
					id:null,
					isAdmin:null
				})
			}
		})
		// console.log(user);
		// console.log(localStorage);
	}, []);

 	return (
 		<>
	 		<UserProvider value = {{user,setUser,unsetUser}}>
	 		{/*dlwang curly brace, value and javascript expression*/}
		 		<Router>
			 		<AppNavbar/>
			 		<Container>
				 		<Routes>
				 			<Route exact path= "/" element={<Home/>}/>
				 			<Route exact path="/exclusives" element={<Exclusives/>}/>
				 			<Route exact path="/users" element={<ViewUsers/>}/>
				 			<Route exact path="/myorders" element={<UserOrder1/>}/>
				 			<Route exact path="/orders" element={<Order1/>}/>
				 			<Route exact path="/products" element={<Product1/>}/>
				 			<Route exact path="/productView/:productId" element={<ProductView/>}/>
				{/* 			<Route exact path="/adminView/:productId" element={<AdminViewp/>}/>*/}
				 			<Route exact path="/register" element={<Register/>}/>
				 			<Route exact path="/profile" element={<Profile/>}/>
				{/* 			<Route path="/orders/getUserOrders" element={<Order/>}/>*/}
				 			{/*<Route path ="/products/create" element={<AddProduct/>}/>*/}
				 			{/*<Route exact path="/myCart" element={<CartView/>}/>*/}
				 			<Route exact path="/login" element={<Login/>}/>
				 			<Route exact path="/logout" element={<Logout/>}/>
				 			<Route exact path="*" element={<Error/>}/>
				 		</Routes>
		 			</Container>
		 		</Router>
		 	</UserProvider>
	 	</>
 		)
}

export default App;
