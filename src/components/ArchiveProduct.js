import {useContext} from 'react';
import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ArchiveProduct({ product, isActive, fetchData}) {
	console.log(fetchData);
	console.log(product);
	console.log(isActive);

	const { user } = useContext(UserContext);
	console.log(user);

	const token = localStorage.getItem("token")

	const archiveToggle = (productId) => {
		fetch(`https://pop-shopp-api.onrender.com/products/archive/${ productId }`,{
			method: 'DELETE',
			headers: {

				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			if(data) {
				Swal.fire({
					title: 'Yey!',
					icon: 'success',
					text: 'Product successfully archived!'
				})
				fetchData()
			}
			// else {
			// 	Swal.fire({
			// 		title: 'Unable to Archive',
			// 		icon: 'error',
			// 		text: 'Failed.'
			// 	})
			// 	fetchData()
			// }
		})
	}

	const activateToggle = (productId) => {
		fetch(`https://pop-shopp-api.onrender.com/products/activate/${ productId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: 'Yey!',
					icon: 'success',
					text: 'Product successfully activated!'
				})
				fetchData()
			}

			// else {
			// 	Swal.fire({
			// 		title: 'Unable to Activate',
			// 		icon: 'error',
			// 		text: 'Something went wrong'
			// 	})
			// 	fetchData()
			// }
		})
	}

	return(

		<>

			{isActive  ?
				<Button variant="outline-danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>

				:

				<Button variant="outline-success" size="sm" onClick={() => activateToggle(product)}>Activate</Button>

			}
			
		</>
		)
}

//no problems now, ok with PS