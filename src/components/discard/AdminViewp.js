import {useState, useEffect, useContext} from 'react';
import {Container,Row,Col,Card,Button} from 'react-bootstrap';
import {useParams,useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function AdminView(){

		const {user} = useContext(UserContext);
		//allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course 
		const history = useNavigate();

		const [name,setName] = useState("");
		const [description, setDescription] = useState("");
		const [price, setPrice] = useState(0);

		//useParams hook allows use to retrieve the courseId passed via the URL
		const {productId} = useParams();

		//make buy function
		const archive = (productId) => {
			fetch('http://localhost:4000/products/archive/${productId}',{
				method:'DELETE',
				headers:{
					'Content-Type':'application/json',
					Authorization:`Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productId: productId
				})
			})
			.then(res=>res.json())
			.then(data=>{
				console.log(data);

				if(data){
				Swal.fire({
					title:"Successfully Archived!",
					icon:"success",
					text:"Thank you for buying!"
				});
				history("/products");
				}else{
				Swal.fire({
					title:"Something went wrong!",
					icon: "error",
					text: "Please try again later or check your credentials!"
				});
			};
		});
	};
	//failed
		useEffect(()=>{
			console.log(productId)
			fetch(`http://localhost:4000/products/getSingleProduct/${productId}`)
			.then(res=>res.json())
			.then(data=>{
				console.log(data);
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
			});
		},[productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg ={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Card.Subtitle>You got a free shipping discount!</Card.Subtitle>
							<Card.Text>Claim it here.</Card.Text>
							{user.id !==null?
								<Button variant="primary" onClick={()=>archive(productId)}>Archive Now</Button>
								:
								<Link className="btn btn-danger" to="/login">Log In</Link>}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		)
}