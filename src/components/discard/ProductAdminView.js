import {useState, useEffect} from 'react';
import {Row, Col, Card, Button, Table} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductAdminView({productProp}) {

	
	function archiveProduct (productId, isActive) {
		fetch(`http://localhost:4000/archive/${productId}`, {
			method: "DELETE",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: !isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})

	}


	function enableProduct (productId, isActive) {
		fetch(`http://localhost:4000/activate/${productId}`, {
			method: "PUT",
			headers: {
				'Content-Type': "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
	}

	const { name, description, price, _id, isActive } = productProp;
	return (
		
			<Row>
				<Col>
					<Table striped>
					      <thead>
					        <tr>
					          <th>Product Name</th>
					          <th>Description</th>
					          <th>Price</th>
					          <th>Availability</th>
					          <th>Actions</th>
					        </tr>
					      </thead>
					      <tbody>
					        <tr key={_id}>
					          <td>{name}</td>
					          <td>{description}</td>
					          <td>{price}</td>
					          <td>
					          		{isActive
					          			? <span>Available</span>
										: <span>Unavailable</span>
					          		}
					          </td>

					          <td>
					          		{isActive
					          			? <Button className="m-3" variant="outline-danger" onClick={(e) => archiveProduct(_id, isActive)}>Archive</Button>
					          			: <Button className="m-3" variant="oultine-success" onClick={(e) => archiveProduct(_id, isActive) }>Enable</Button>
					          		}
					          </td>

					        </tr>
					      </tbody>
					    </Table>
					

				</Col>
			</Row>

		)

}