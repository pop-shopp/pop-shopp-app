import { useState, useEffect } from 'react';
import {Button,Table} from 'react-bootstrap';
import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';
// import Order1 from '../pages/Order1';
import {Link} from 'react-router-dom';


export default function AdminView(props) {

	console.log(props)
	const { productsData, fetchData } = props;

	const [ products, setproducts ] = useState([])
	console.log(fetchData)

	useEffect(() => {

		const productsArr = productsData.map(product => {
			return(
				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "Available" : "Unavailable"}
					</td>
					<td>
						<EditProduct product={product._id} fetchData={fetchData}/>
					</td>
					<td>
						<ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/>
					</td>
				</tr>
				)
		})
		setproducts(productsArr)
	}, [productsData,fetchData])


	return(
		<>
			<div className="text-center my-4">
				<h1>Welcome, Admin!</h1>
				<p>You have exclusive access over Admin Tools.</p>
				<Button  className="me-1" size="sm" style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }} variant="outline-dark" as= {Link} to={`/users`}><span class="material-symbols-rounded">person</span>View All Users</Button>
				<Button  className="me-1" size="sm" style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }} variant="outline-dark" as= {Link} to={`/orders`}><span class="material-symbols-rounded">search</span>View All Orders</Button>
				<AddProduct fetchData={fetchData} />

			</div>
			
			<Table responsive="md" className="colorPurple" style={{textAlign: "center"}}>
				<thead className="text-white">
					<tr>
						<th>Product ID</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th Colspan="2">Admin Tools</th>
					</tr>
				</thead>

				<tbody>
					{ products }
				</tbody>
			</Table>

		</>

		)
}

//no issues, ok for PS