// import {useState,useEffect,useContext} from 'react';
import {Card} from 'react-bootstrap';
// import UserContext from '../UserContext';



export default function OrderCardUser ({orderProp}){
	const {totalAmount, purchasedOn,_id} = orderProp;
	// const {user} = useContext(UserContext);
	return(
				<Card className = "p-3 mb-3">
					<Card.Body className = "colorGreen">
							<Card.Title className="fw-bold">{_id}</Card.Title>
							<Card.Text>
							{totalAmount}
							</Card.Text>
							<Card.Subtitle>Purchased On:</Card.Subtitle>
							<Card.Text>{purchasedOn}</Card.Text>

					</Card.Body>
				</Card>	
		)
};

//ok