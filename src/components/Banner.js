import { Link } from 'react-router-dom'
import {Button, Row, Col} from 'react-bootstrap';
import Gallery from '../components/Gallery';

export default function Banner({data}){

	console.log(data)
    const {title, content, destination1, label1,destination2,label2,destination3,label3,destination4,label4,l1,l2,l3,l4,image} = data;

	return (
		<Row>
				

				<Col xs={12} md={6} lg={4} className = "p-5">
				<Row>
				<h1>{title}</h1>
				<p>{content}</p>
				</Row>
				<Row className = "pb-3">
				<Button id="colorNavbar" div className="outline-dark text-wrap me-2 mb-2" style={{width: "8rem"}} variant="outline-dark" as= {Link} to={destination1}><span class="material-symbols-rounded">{label1}<p class="text-wrap">{l1}</p></span></Button>
				<Button div className="outline-dark text-wrap me-2 mb-2" style={{width: "8rem"}} variant="outline-dark" as= {Link} to={destination3}><span class="material-symbols-rounded">{label3}<p class="text-wrap">{l3}</p></span></Button>
				</Row>
				<Row>
				{/*<Button div className="outline-dark text-wrap me-2" style={{width: "8rem"}} variant="outline-dark" as= {Link} to={destination2}><span class="material-symbols-rounded">{label2}</span><p>Profile</p></Button>*/}
				<Button div className="outline-dark text-wrap me-2 mb-2" style={{width: "8rem"}} variant="outline-dark" as= {Link} to={destination2}><span class="material-symbols-rounded">{label2}<p class="text-wrap">{l2}</p></span></Button>
				
				<Button /*disabled="true"*/ id="colorNavbar" div className="outline-dark text-wrap me-2 mb-2" style={{width: "8rem"}} variant="outline-dark" as= {Link} to={destination4}><span class="material-symbols-rounded">{label4}<p class="text-wrap">{l4}</p></span></Button>
				</Row>
				</Col>

				<Col xs={12} md={4} className = "p-5">
				<p><Link to ="/products"><img alt="PopShop!" src={image}/></Link></p>
				</Col>

				<Col xs={12} md={4} className = "p-5">
				<Gallery/>
				</Col>

				
		</Row>
	)
}

//no prob