import {useState, useEffect, useContext} from 'react';
import {Container,Row,Col,Card,Button} from 'react-bootstrap';
import {useParams,useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView(){

		const {user} = useContext(UserContext);
		//allows us to gain access to methods that will allow us to redirect a user to a different page after enrolling a course 
		const history = useNavigate();

		const [name,setName] = useState("");
		const [description, setDescription] = useState("");
		const [price, setPrice] = useState(0);

		//useParams hook allows use to retrieve the courseId passed via the URL
		const {productId} = useParams();

		//make buy function
		const buyNow = (productId) => {
			fetch('https://pop-shopp-api.onrender.com/orders/order',{
				method:'POST',
				headers:{
					'Content-Type':'application/json',
					Authorization:`Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					productId: productId
				})
			})
			.then(res=>res.json())
			.then(data=>{
				console.log(data);

				if(data){
				Swal.fire({
					title:"Successfully Purchased!",
					icon:"success",
					text:"Thank you for buying!"
				});
				history("/products");
				}else if(data===false){
					Swal.fire({
					title:"You are an Admin!",
					icon:"error",
					text:"You cannot purchase products!"
				});
				}else{
				Swal.fire({
					title:"Something went wrong!",
					icon: "error",
					text: "Please try again later or check your credentials!"
				});
			};
		});
	};

		useEffect(()=>{
			console.log(productId)
			fetch(`https://pop-shopp-api.onrender.com/products/getSingleProduct/${productId}`)
			.then(res=>res.json())
			.then(data=>{
				console.log(data);
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
			});
		},[productId]);

	return(
		<Container className="mt-5">
			<Row>
				<Col lg ={{span:6, offset:3}}>
					<Card>
						<Card.Body style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }}>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							<Card.Subtitle>You got a free shipping discount!</Card.Subtitle>
							<Card.Text>Claim it here.</Card.Text>
							{user.id !==null?
								<>
								<Button className="me-2" style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }} variant="outline-dark" onClick={()=>buyNow(productId)}>Buy Now</Button>
								{/*<Button style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }} variant="outline-dark" disabled="true">Add to Cart</Button>*/}
								</>
								:
								<Link className="btn btn-danger" to="/login">Log In</Link>}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		)
}

//okay