import { useContext, useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
//working
export default function EditProduct({ product, fetchData }){
	console.log(fetchData);
	console.log(product);

	

	const [productId, setProductId] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	const { user } = useContext(UserContext);
	console.log(user);

	const [ showEdit, setShowEdit ] = useState(false)
	const token = localStorage.getItem("token")


	const openEdit = (productId) => {
		fetch(`https://pop-shopp-api.onrender.com/products/getSingleProduct/${ productId }`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProductId(data._id)
			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
		})

		setShowEdit(true)
	}


	
	const closeEdit = () => {
		setShowEdit(false)
		setName('')
		setDescription('')
		setPrice(0)
	}


	const editProduct = (e, productId) => {
		e.preventDefault();
		console.log(productId)
		fetch(`https://pop-shopp-api.onrender.com/products/${ productId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data){
				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product successfully updated'
				})
				fetchData()
				closeEdit()
			}else {
				Swal.fire({
					title: 'Something Went Wrong!',
					icon: 'error',
					text: 'Please try again'
				})

				fetchData()
				closeEdit()
			}
		})
	}


	return(
		<>
			<Button variant="outline-primary" size="sm" onClick={() => openEdit(product)}>Update</Button>

			<Modal show={showEdit} onHide={closeEdit}>
				<Form className="colorBlue" onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control 
							      type="text"
							      required
							      value={name}
							      onChange={e => setName(e.target.value)}
							 />
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
							      type="text"
							      required
							      value={description}
							      onChange={e => setDescription(e.target.value)}
							 />
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control 
							      type="number"
							      required
							      value={price}
							      onChange={e => setPrice(e.target.value)}
							 />
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="outline-secondary" onClick={closeEdit}>Close</Button>
						<Button variant="outline-success" type="submit">Submit</Button>
					</Modal.Footer>

				</Form>
			</Modal>

		</>


		)
}
//no prob noww