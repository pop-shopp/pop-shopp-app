// import {useState,useEffect,useContext} from 'react';
import {Card,Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
// import UserContext from '../UserContext';



export default function ProductCard ({productProp}){
	const {name, description, price,_id} = productProp;
	// const {user} = useContext(UserContext);
	return(
				<Card className = "p-3 mb-3">
					<Card.Body style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }}>
							<Card.Title className="fw-bold">{name}</Card.Title>
							<Card.Subtitle>Product Description:</Card.Subtitle>
							<Card.Text>
							{description}
							</Card.Text>
							<Card.Subtitle>Product Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>

							<Button  className="me-5" size="sm" style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }} variant="outline-dark" as= {Link} to={`/productView/${_id}`}><span class="material-symbols-rounded">search</span>View Details</Button>
						{/*	<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>*/}

							{/*<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>*/}

					</Card.Body>
				</Card>	
		)
};

//ok