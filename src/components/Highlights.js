import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights (){
	return(
		<Row className = "mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
					<Card.Body className= "colorPurple">
						<Card.Title>
							<h2 style={{textAlign: "center"}}>K-pop Merch</h2>
							<img className="d-block w-100" src="https://cdn.shopify.com/s/files/1/0325/4101/6201/products/product-image-1467786792_300x300.jpg?v=1596432995" alt="New Lightstick!"/>
						</Card.Title>
						<Card.Text>
						
							BTS, the first artist to join the ‘FRIENDS CREATORS’ Project, is a creator that shows connection in terms of the global character brand LINE FRIENDS’ keywords: Global, Millennials and Trending. BTS has high popularity and strong influence in the world, and are great story tellers of our time who can share the story of ‘development in IP’.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
					<Card.Body className= "colorPurple">
						<Card.Title>
							<h2 style={{textAlign: "center"}}>Anime Merch</h2>
						</Card.Title>
						<Card.Text>
						<img className="d-block w-100" src="https://jujutsukaisen.store/wp-content/uploads/2022/05/best-selling-xuan-2.gif" alt="JJK!"/>
							It seems like finding the perfect gift for your Anime lover one’s difficult problems. But don’t worry, our store is offering Jujutsu Kaisen Hoodie with a strong deal. It will make an excellent highlight gift for your special one.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className = "cardHighlight p-3">
					<Card.Body className= "colorPurple">
						<Card.Title>
							<h2 style={{textAlign: "center"}}>Games and Tech</h2>
						</Card.Title>
						<Card.Text>
						<img className="d-block w-100" src="https://64.media.tumblr.com/530cd590907fed60c4620b9e5bc75000/7307b14ffd4f8e6f-0b/s540x810/2a7b4b8536cfecee7fcfdcb4b1c60e39937dd3ec.pnj" alt="New Released Album!"/>
							Star Guardian is a series of parallel universe skins in League of Legends that follow the story of a group of high school students who choose to become cosmic protectors and must battle cosmic enemies threatening the universe. However, they consequently learn of the anguish and perils associated with their newfound roles as self-doubt, desperation and the daily life of a high school student.
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
		)
};
//ok