
import {Card} from 'react-bootstrap';




export default function ViewUsersCard ({uservProp}){
	const {firstName,lastName,email,mobileNo,_id} = uservProp;
	return(
				<Card className = "p-3 mb-3">
					<Card.Body className = "colorGreen">
							<Card.Title className="fw-bold">{firstName} {lastName}</Card.Title>
							<Card.Subtitle>{_id}</Card.Subtitle>
							<Card.Subtitle>Contact Details:</Card.Subtitle>
							<Card.Text>
							{email}, {mobileNo}
							</Card.Text>

					</Card.Body>
				</Card>	
		)
};

//ok