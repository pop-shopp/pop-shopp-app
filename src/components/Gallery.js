import Carousel from 'react-bootstrap/Carousel';

export default function Gallery() {
  return (
    <Carousel>

      <Carousel.Item interval={9000}>
        <img
          className="d-block w-100"
          src="https://www.soundtrack.net/img/album/30600.jpg"
          alt="New Released Album!"
        />
        <Carousel.Caption>
          <h3>New Release!</h3>
          <p>League of Legends:Wild Rift Album</p>
        </Carousel.Caption>

      </Carousel.Item>
      <Carousel.Item interval={9000}>
        <img
          className="d-block w-100"
          src="https://upload.wikimedia.org/wikipedia/en/thumb/6/62/Ly-her.jpg/220px-Ly-her.jpg"
          alt="New Stocks"
        />
        <Carousel.Caption>
          <h3>Stocks are now Available!</h3>
          <p>Love Yourself:Her</p>
        </Carousel.Caption>
      </Carousel.Item>
      <Carousel.Item interval={9000}>
        <img
          className="d-block w-100"
          src="http://images.goodsmile.info/cgm/images/product/20220623/12875/100446/medium/ab5431388d9ad0fa4d69e04f1f227ce2.jpg"
          alt="Coming Soon"
        />
        <Carousel.Caption>
          <h3>Coming Soon!</h3>
          <p>SpyxFamily Nendoroid!</p>
        </Carousel.Caption>
      </Carousel.Item>
    </Carousel>
  );
}

//ok