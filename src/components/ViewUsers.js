import {useState, useEffect} from 'react';
import ViewUsersCard from '../components/ViewUsersCard';

export default function ViewAllUsers () {

	const [users,setUsers] = useState([])
	const token = localStorage.getItem("token")

	useEffect(()=>{
		fetch("https://pop-shopp-api.onrender.com/users",{
			method: "GET",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			setUsers(data.map(user=>{
				return(
					<ViewUsersCard key={user._id} uservProp = {user}/>
				)
			}))


		})

	})
	
	return(
		<>
			<h1 style={{textAlign: "center"}}>Registered Users:</h1>
			{users}
		</>
		)
}

//ok