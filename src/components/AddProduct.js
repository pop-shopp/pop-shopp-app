import {useState} from 'react';
import {Form,Button,Modal} from 'react-bootstrap';
import Swal from 'sweetalert2';
// import UserContext from '../UserContext';

export default function AddProduct({fetchData}) {
	// console.log(fetchData);
	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	// const { user } = useContext(UserContext);
	// console.log(user)

	const [ create, setCreate ] = useState(false);
	const token = localStorage.getItem("token")

	const openAdd = () => setCreate(true);
	const closeAdd = () => setCreate(false); 


	const AddProduct = (e) => {
		e.preventDefault();

		fetch('https://pop-shopp-api.onrender.com/products/create', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`
			},
				body: JSON.stringify({	
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)

			if(data){
				Swal.fire({
					title: 'Yey',
					icon: 'success',
					text: 'Product successfully added'
				})

				fetchData()
				closeAdd()
	
				
			}else {
				Swal.fire({
					title: 'Meh',
					icon: 'error',
					text: 'Please try again'
				})

				fetchData()
			}

	
			setName('')
			setDescription('')
			setPrice(0)
		})


	}

	return(
		<>
			<Button  size="sm" style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }} variant="outline-dark" onClick={openAdd}>Add New Product<span class="material-symbols-rounded">add_circle</span></Button>

			<Modal show={create} onHide={closeAdd}>
				<Form style={{ background: 'linear-gradient(to right, rgba(102, 126, 234, 0.5), rgba(118, 75, 162, 0.5))' }} onSubmit={e => AddProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control 
							      type="text"
							      required
							      value={name}
							      onChange={e => setName(e.target.value)}
							 />
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control 
							      type="text"
							      required
							      value={description}
							      onChange={e => setDescription(e.target.value)}
							 />
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control 
							      type="number"
							      required
							      value={price}
							      onChange={e => setPrice(e.target.value)}
							 />
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant="outline-dark" onClick={closeAdd}>Close</Button>
						<Button variant="outline-success" type="submit">Submit</Button>
					</Modal.Footer>

				</Form>
			</Modal>
		</>
	)
}

//no issues, ok for poppshopp